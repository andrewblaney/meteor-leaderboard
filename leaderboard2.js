
// Create a MongoDB Collection
PlayersList = new Mongo.Collection('players');

// Code that only runs on the client (within the web browser)
if(Meteor.isClient){

    // "Catch" the selection of data from the Meteor.publish function
    Meteor.subscribe('thePlayers');

    // Helper functions execute code within templates
    Template.leaderboard.helpers({
        'player': function(){

            // Get the ID of the current user
            var currentUserId = Meteor.userId();

            // Retrieve data that belongs to the current user
            return PlayersList.find({createdBy: currentUserId},
                {sort: {score: -1, name: 1}});

        },
        'selectedClass': function(){

            // Get the ID of the player being iterated through
            var playerId = this._id;

            // Get the ID of the player that's been clicked
            var selectedPlayer = Session.get('selectedPlayer');

            // Do these IDs match?
            if(playerId == selectedPlayer){

                // Return a CSS class
                return "selected"

            }

        },
        'showSelectedPlayer': function(){

            // Get the ID of the player that's been clicked
            var selectedPlayer = Session.get('selectedPlayer');

            // Retrieve a single document from the collection
            return PlayersList.findOne(selectedPlayer)

        }
    });

    Template.updatePlayer.helpers({

        'showUpdatePlayer': function(){
            var display = Session.get('showUpdatePlayer');
            if (display) {
                return 'showUpdatePlayer';
            }
        }
    });

    Template.updatePlayer.events({
        'submit form': function(event){

            // Prevent the browser from applying default behaviour to the form
            event.preventDefault();

            // Get the value from the "playerName" text field
            var newPlayerName = event.target.playerNameUpdate.value;
            var newPlayerScore = parseInt(event.target.playerScoreUpdate.value);
            var selectedPlayer = Session.get('selectedPlayer');
            // Call a Meteor method and pass through a name
            Meteor.call('modifyPlayerName', selectedPlayer, newPlayerName);
            Meteor.call('replacePlayerScore', selectedPlayer, newPlayerScore);
            event.target.playerNameUpdate.value='';
            event.target.playerScoreUpdate.value='';
        }
    });

    // Events trigger code when certain actions are taken
    Template.leaderboard.events({
        'click .player': function(){

            // Retrieve the unique ID of the player that's been clicked
            var playerId = this._id;

            // Create a session to store the unique ID of the clicked player
            Session.set('selectedPlayer', playerId);

        },
        'click .increment': function(){

            // Get the ID of the player that's been clicked
            var selectedPlayer = Session.get('selectedPlayer');

            // Call a Meteor method and pass through selected player's ID and a value to increment by
            Meteor.call('incPlayerScore', selectedPlayer, 5);

        },
        'click .decrement': function(){

            // Get the ID of the player that's been clicked
            var selectedPlayer = Session.get('selectedPlayer');

            // Call a Meteor method and pass through selected player's ID and a value to decrement by
            Meteor.call('incPlayerScore', selectedPlayer, -5);

        },
        'click .remove': function(){

            // Get the ID of the player that's been clicked
            var selectedPlayer = Session.get('selectedPlayer');

            // Call a Meteor method and pass through selected player's ID
            Meteor.call('removePlayerData', selectedPlayer);

        },
        'click .update': function(){
            var x = Session.get('showUpdatePlayer');
            Session.set('showUpdatePlayer', !x);
        }
    });

    Template.addPlayerForm.events({
        'submit form': function(event){

            // Prevent the browser from applying default behaviour to the form
            event.preventDefault();

            // Get the value from the "playerName" text field
            var playerName = event.target.playerName.value;
            var playerScore = parseInt(event.target.playerScore.value);

            // Call a Meteor method and pass through a name
            Meteor.call('insertPlayerData', playerName, playerScore);

            event.target.playerName.value='';
            event.target.playerScore.value = '';

        }
    });

}

// Code that only runs on the server (where the application is hosted)
if(Meteor.isServer){

    // Transmit a selection of data into the ether
    Meteor.publish('thePlayers', function(){

        // Get the ID of the current user
        var currentUserId = this.userId;

        // Return players "owned" by the current user
        return PlayersList.find({createdBy: currentUserId})

    });

    // Methods execute on the server after being triggered from the client
    Meteor.methods({
        'insertPlayerData': function(playerName, playerScore){

            // Get the ID of the current user
            var currentUserId = Meteor.userId();

            // Insert the data of a new player
            PlayersList.insert({
                name: playerName,
                score: playerScore,
                createdBy: currentUserId
            });

        },
        'removePlayerData': function(selectedPlayer){

            // Remove a document from the collection
            PlayersList.remove(selectedPlayer);

        },
        'incPlayerScore': function(selectedPlayer, scoreValue){

            // Update a document and either increment or decrement the score field
            PlayersList.update(selectedPlayer, {$inc: {score: scoreValue} });

        },

        'replacePlayerScore': function(selectedPlayer, scoreValue){

            // Update a document and either increment or decrement the score field
            PlayersList.update(selectedPlayer, {$set: {score: scoreValue} });

        },
        'modifyPlayerName': function(selectedPlayer, newPlayerName){
            // Update a document change the name field
            PlayersList.update(selectedPlayer, {$set: {name: newPlayerName} });
        }
    });

}